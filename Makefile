.PHONY: lint unit build test

lint:
	@cargo fmt -- --write-mode=diff

unit:
	@cargo test

build:
	@cargo build --release

test:
	@echo "success"
