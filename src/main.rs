fn test_string() -> &'static str {
    "true"
}

fn main() {
    println!("{}", test_string());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn correct_test_string() {
        assert_eq!("true", test_string());
    }
}
